package br.com.otgmobile.skynetandroid.util;

public class ConstUtil {
	
	public static final String LARGE_PNG = "_large.png";
	public static final String IMAGE_PNG = "_image.png";
	public static final String MEDIDUM_PNG = "_medidum.png";
	public static final String THUMB_PNG = "_thumb.png";
	public static final String ICON_PNG = "_icon.png";
	public static final String SERIALIZABLE_NAME = "serializable";
	public static final String POSITION = "position";

}
