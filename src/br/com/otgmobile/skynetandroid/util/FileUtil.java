package br.com.otgmobile.skynetandroid.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class FileUtil {
	
	public static Bitmap openImage(Context context, String url){
		FileInputStream fis = null;
			try {
				fis = context.openFileInput(url);
				Bitmap bitmap = BitmapFactory.decodeStream(fis);
				return bitmap;
			} catch (FileNotFoundException e) {
				LogUtil.e("Imagem não encotrada", e);
				return null;
			}
	}

}
