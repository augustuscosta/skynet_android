package br.com.otgmobile.skynetandroid.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Session {
	

	public static final String PREFS 						 = "SESSION_PREFS";
	public static final String SERVER_PREFS_KEY 			 = "SERVER_PREFS_KEY";
	public static final String SOCKET_SERVER_PREFS_KEY 		 = "SOCKET_SERVER_PREFS_KEY";
	public static final String LAST_SYNC					 = "LAST_SYNC";

	private static SharedPreferences settings;

	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static String getDeviceId(Context context){
		TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getServer(Context context){				
		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://23.23.89.95:3000/empresas/conteudo/zipcodebrand");
	}
	
	public static void setSocketServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SOCKET_SERVER_PREFS_KEY, server);
		editor.commit();
	}
	

}
