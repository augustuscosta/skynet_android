package br.com.otgmobile.skynetandroid.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.LogUtil;

import com.j256.ormlite.dao.Dao;

public class ConteudoProvider {

	private  Dao<Conteudo, Long> dao;
	private Context context;

	public ConteudoProvider(Context context){
		this.context = context;
	}

	public  List<Conteudo> getChildConteudos(Conteudo conteudo){
		try {
			return dao().queryForEq("parentId", conteudo.getId());
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar os conteudos alinhados", e);
			return null;
		}finally{
			try {
				dao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public Conteudo getParentConteudo(Conteudo conteudo){
		try {
			return dao().queryForId(conteudo.getParentId());
		}  catch (SQLException e) {
			LogUtil.e("erro ao pegar os conteudo pai", e);
			return null;
		}finally{
			try {
				dao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public  Conteudo getRootConteudo(){
		List<Conteudo> todos = null;
		try {
			todos = dao().queryForAll();
			for(Conteudo conteudo : todos){
				if(conteudo.getParentId() == null){
					return conteudo;
				}
			}
			return null;
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar os conteudo pai", e);
			return null;
		}finally{
			try {
				dao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}


	}

	private Dao<Conteudo, Long> dao(){
		if(dao == null){
			try {
				dao = DatabaseHelper.getDatabase(context).getDao(Conteudo.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar o dao", e);
			}
		}

		return dao;
	}

}
