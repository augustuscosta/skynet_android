package br.com.otgmobile.skynetandroid.database;


import br.com.otgmobile.skynetandroid.model.Conteudo;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

	private static final Class<?>[] classes = new Class[] {
		Conteudo.class
	};

	public static void main(String[] args) throws Exception {
		writeConfigFile("ormlite_config.txt", classes);
	} 
}