package br.com.otgmobile.skynetandroid.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.LogUtil;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	private static final String DATABASE_NAME = "Skynet.db";
	private static final int DATABASE_VERSION = 1;
	private static DatabaseHelper databaseHelper;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		try {
			TableUtils.createTable(arg1, Conteudo.class);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar as tabelas do banco", e);
		}
		DaoManager.clearDaoCache();
	}
	
	
	public static OrmLiteSqliteOpenHelper getDatabase(Context context){
		if(databaseHelper == null){
			databaseHelper = new DatabaseHelper(context);
		}
		
		return databaseHelper;
	}
	

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

}
