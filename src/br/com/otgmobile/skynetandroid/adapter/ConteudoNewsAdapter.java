package br.com.otgmobile.skynetandroid.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.adapter.holders.ConteudoNewsHolder;
import br.com.otgmobile.skynetandroid.model.Conteudo;

public class ConteudoNewsAdapter extends BaseAdapter {
	
	private List<Conteudo> conteudos;
	private Activity context;
	
	public ConteudoNewsAdapter(List<Conteudo> conteudos,Activity context ){
		this.conteudos = conteudos;
		this.context = context;
	}

	@Override
	public int getCount() {
		return conteudos == null ? 0 : conteudos.size();
	}

	@Override
	public Conteudo getItem(int position) {
		return conteudos == null ? null : conteudos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = null;
		Conteudo conteudo = conteudos.get(position);
		ConteudoNewsHolder holder;
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.conteudo_news_row, null);
			holder = new ConteudoNewsHolder();
			holder.newsTitle = (TextView) row.findViewById(R.id.newsTitle);
			holder.newsTumb = (ImageView) row.findViewById(R.id.newsTumb);
		}else{
			row = convertView;
			holder = (ConteudoNewsHolder) row.getTag();
		}
		
		holder.drawRow(conteudo, context);
		row.setTag(holder);
		
		return row;
	}

}
