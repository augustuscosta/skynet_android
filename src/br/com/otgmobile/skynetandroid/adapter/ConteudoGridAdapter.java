package br.com.otgmobile.skynetandroid.adapter;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;

public class ConteudoGridAdapter extends BaseAdapter{
	
	private List<Conteudo> conteudos;
	private Activity context;
	
	
	
	public ConteudoGridAdapter(Activity context, List<Conteudo> conteudos){
		this.conteudos = conteudos;
		this.context = context;
	}

	@Override
	public int getCount() {
		return conteudos == null? 0: conteudos.size();
	}

	@Override
	public Conteudo getItem(int position) {
		return conteudos == null? null : conteudos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(200 ,200));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
		Bitmap bitmap = FileUtil.openImage(context, conteudos.get(position).getId()+ConstUtil.THUMB_PNG);
		imageView.setImageBitmap(bitmap);
		imageView.setTag(conteudos.get(position));
		return imageView;
	}
	
	

}
