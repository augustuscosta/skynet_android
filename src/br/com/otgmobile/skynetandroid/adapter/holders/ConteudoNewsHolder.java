package br.com.otgmobile.skynetandroid.adapter.holders;

import java.io.FileInputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class ConteudoNewsHolder {
	
	public ImageView newsTumb;
	public TextView newsTitle;
	
	public void drawRow(Conteudo conteudo, Activity context){
		FileInputStream fis = null;
		try {
			fis = context.openFileInput(conteudo.getId()+ConstUtil.THUMB_PNG);
			Bitmap bitmap = BitmapFactory.decodeStream(fis);
			newsTumb.setImageBitmap(bitmap);
			}
		catch(Exception e){
			LogUtil.e("erro ao criar imagem ", e);
		}
		
		newsTitle.setText(conteudo.getTitulo());
		
		
	}

}
