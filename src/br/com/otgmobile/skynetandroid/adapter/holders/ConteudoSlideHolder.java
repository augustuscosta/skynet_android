package br.com.otgmobile.skynetandroid.adapter.holders;

import java.io.FileInputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class ConteudoSlideHolder {
	
	public ImageView touchImageView;
	
	public void drawImage(Conteudo conteudo,Activity context){
		FileInputStream fis = null;
		try {
			fis = context.openFileInput(conteudo.getId()+ConstUtil.IMAGE_PNG);
			Bitmap bitmap = BitmapFactory.decodeStream(fis);
			touchImageView.setImageBitmap(bitmap);
			}
		catch(Exception e){
			LogUtil.e("erro ao criar imagem ", e);
		}
	}

}
