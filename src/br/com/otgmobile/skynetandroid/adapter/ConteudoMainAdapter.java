package br.com.otgmobile.skynetandroid.adapter;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;

public class ConteudoMainAdapter extends BaseAdapter{
	
	private List<Conteudo> conteudos;
	private Activity context;
	
	
	
	public ConteudoMainAdapter(Activity context, List<Conteudo> conteudos){
		this.conteudos = conteudos;
		this.context = context;
	}

	@Override
	public int getCount() {
		return conteudos == null? 0: conteudos.size();
	}

	@Override
	public Conteudo getItem(int position) {
		return conteudos == null? null : conteudos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView i = new ImageView(context);
		Bitmap bitmap = FileUtil.openImage(context, conteudos.get(position).getId()+ConstUtil.IMAGE_PNG);
		i.setImageBitmap(bitmap);
		i.setLayoutParams(new Gallery.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		i.setTag(conteudos.get(position));
		return i;
	}
	
	
	

}
