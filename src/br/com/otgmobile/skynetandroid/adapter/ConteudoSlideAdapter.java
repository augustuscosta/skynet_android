package br.com.otgmobile.skynetandroid.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.adapter.holders.ConteudoSlideHolder;
import br.com.otgmobile.skynetandroid.model.Conteudo;

public class ConteudoSlideAdapter extends BaseAdapter{
	
	private List<Conteudo> conteudos;
	private Activity context;
	
	
	
	public ConteudoSlideAdapter(Activity context, List<Conteudo> conteudos){
		this.conteudos = conteudos;
		this.context = context;
	}

	@Override
	public int getCount() {
		return conteudos == null? 0: conteudos.size();
	}

	@Override
	public Conteudo getItem(int position) {
		return conteudos == null? null : conteudos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View i;
		ConteudoSlideHolder holder;
		Conteudo conteudo = conteudos.get(position);
		if(convertView == null){
			holder = new ConteudoSlideHolder();
			LayoutInflater inflater = context.getLayoutInflater();
			i = inflater.inflate(R.layout.slide_holder, null);
			holder.touchImageView = (ImageView) i.findViewById(R.id.touchImage);
		}else{
			i =  convertView;
			holder = (ConteudoSlideHolder) i.getTag();
		}
		
		holder.drawImage(conteudo, context);
		i.setTag(holder);
		
		return i;
	}
	
	
	

}
