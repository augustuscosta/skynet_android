package br.com.otgmobile.skynetandroid.activity;

import java.util.List;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.activity.IntentProviders.IntentProvider;
import br.com.otgmobile.skynetandroid.adapter.ConteudoClickHandler;
import br.com.otgmobile.skynetandroid.adapter.ConteudoGridAdapter;
import br.com.otgmobile.skynetandroid.database.ConteudoProvider;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;

public class GridGalleryActivity extends GenericGalleryActivity<DatabaseHelper> implements ConteudoClickHandler{
	
	@InjectView(R.id.gridGallery) GridView gridGallery;
	@InjectView(R.id.background) LinearLayout background;
	private Conteudo mainConteudo;
	private List<Conteudo> conteudos;
	private ConteudoProvider provider;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grid_gallery);
		handleIntent(getIntent());
		
	}

	private void handleIntent(Intent intent) {
		Conteudo conteudo =(Conteudo) intent.getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		mainConteudo = conteudo;
		loadChildrenAndFillGallery(conteudo);		
	}

	private void loadChildrenAndFillGallery(Conteudo conteudo) {
			Conteudo conteudoMain = provider().getRootConteudo();
        	Bitmap bitmap = FileUtil.openImage(this, conteudoMain.getId() + ConstUtil.IMAGE_PNG);
        	BitmapDrawable bmD = new BitmapDrawable(bitmap);
        	background.setBackgroundDrawable(bmD);
			conteudos = provider().getChildConteudos(mainConteudo);
			gridGallery.setAdapter(new ConteudoGridAdapter(this, conteudos));
			gridGallery.setOnItemClickListener(conteudoSelectedListener);
	}

	@Override
	public void conteudoSelected(Conteudo conteudo, int position) {
		Intent intent = IntentProvider.getIntentFromConteudo(this, conteudo, position);
		startActivity(intent);
	}
	
	private OnItemClickListener conteudoSelectedListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			conteudoSelected(conteudos.get(arg2),arg2);
			
		}
	};
	
	private ConteudoProvider provider(){
		if(provider == null){
			provider = new ConteudoProvider(this);
		}
		
		return provider;
	}

}
