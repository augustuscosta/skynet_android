package br.com.otgmobile.skynetandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.DeveloperKey;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

public class ConteudoVideoActivity extends YouTubeBaseActivity implements OnInitializedListener {
	
	private YouTubePlayerView youTubePlayer;
	private Conteudo conteudo;
	private String videoId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conteudo_video);
		youTubePlayer = (YouTubePlayerView) findViewById(R.id.playerView);
		handleIntent(getIntent());
	}
	

	private void handleIntent(Intent intent) {
		conteudo = (Conteudo) intent.getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		intiPlayerWithVideoFromConteudo();
	}

	private void intiPlayerWithVideoFromConteudo() {
		if(conteudo == null || conteudo.getUrl() == null){	
		return;	
		}
		
		 videoId = conteudo.getUrl().substring(conteudo.getUrl().lastIndexOf("/")+1);
		youTubePlayer.initialize(DeveloperKey.DEVELOPER_KEY, this);
	}

	@Override
	public void onInitializationFailure(Provider provider,YouTubeInitializationResult youTubeInitializationResult) {
		
		
	}

	@Override
	public void onInitializationSuccess(Provider provider, YouTubePlayer player,boolean wasRestored) {
		if(!wasRestored){
			player.loadVideo(videoId);						
		}
	}

}
