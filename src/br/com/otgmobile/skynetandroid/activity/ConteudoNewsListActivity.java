package br.com.otgmobile.skynetandroid.activity;

import java.util.List;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.activity.IntentProviders.IntentProvider;
import br.com.otgmobile.skynetandroid.adapter.ConteudoNewsAdapter;
import br.com.otgmobile.skynetandroid.database.ConteudoProvider;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class ConteudoNewsListActivity extends GenericListActivity<DatabaseHelper>{
	
	@InjectView(R.id.container) LinearLayout container;
	private List<Conteudo> conteudos;
	private Conteudo mainConteudo;
	private ConteudoProvider provider;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conteudo_news_list);
		handleIntent();
	}


	private void handleIntent() {
		mainConteudo = (Conteudo) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		try{
			Conteudo conteudoMain = provider().getRootConteudo();
			conteudos = provider().getChildConteudos(mainConteudo);
			Bitmap bitmap = FileUtil.openImage(this, conteudoMain.getId() + ConstUtil.IMAGE_PNG);
			BitmapDrawable bmD = new BitmapDrawable(bitmap);
			container.setBackgroundDrawable(bmD);
			setListAdapter(new ConteudoNewsAdapter(conteudos, this));			
		}catch(Exception e){
			LogUtil.e("erro ao carregar conteudos", e);
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		conteudoSelected(conteudos.get(position));
	}
	
	public void conteudoSelected(Conteudo conteudo) {
		Intent intent = IntentProvider.getIntentFromConteudo(this, conteudo, null);
		if(intent!= null){
			startActivity(intent);
		}
	}
	
	private ConteudoProvider provider(){
		if(provider == null){
			provider = new ConteudoProvider(this);
		}
		
		return provider;
	}

}
