package br.com.otgmobile.skynetandroid.activity;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;

import roboguice.inject.InjectView;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class ConteudoNewsArticleActivity extends GenericActivity<DatabaseHelper> {
	
	@InjectView(R.id.container) LinearLayout container;
	@InjectView(R.id.newsThumb) ImageView newsThumb;
	@InjectView(R.id.newsText) WebView newsText;
	private Dao<Conteudo,Long> dao;
	private String mime = "text/html";
	private String encoding = "utf-8";
	
	private Conteudo mainConteudo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.article_viewer);
		initializeDao();
		handleIntent();
	}

	private void handleIntent() {
		mainConteudo = (Conteudo) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		try{
			Conteudo conteudoMain = dao.queryForId((long) 1);
			Bitmap bitmap = FileUtil.openImage(this, conteudoMain.getId() + ConstUtil.IMAGE_PNG);
			BitmapDrawable bmD = new BitmapDrawable(bitmap);
			container.setBackgroundDrawable(bmD);
			populate();
		}catch(Exception e){
			LogUtil.e("erro ao pegar conteudo", e);
		}
	}
	
	private void populate() {
		Bitmap bitmap = FileUtil.openImage(this, mainConteudo.getId() + ConstUtil.THUMB_PNG);
		newsThumb.setImageBitmap(bitmap);
		String page = mainConteudo.getDescription();
		newsText.loadDataWithBaseURL(null, page, mime, encoding, null); ;
	}

	private void initializeDao() {
		try {
			dao = getHelper().getDao(Conteudo.class);
		} catch (SQLException e) {
			LogUtil.e("erro ao instanciar o dao", e);
		}	
	}

}
