package br.com.otgmobile.skynetandroid.activity;

import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Gallery;
import android.widget.LinearLayout;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.adapter.ConteudoSlideAdapter;
import br.com.otgmobile.skynetandroid.database.ConteudoProvider;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;

public class SlideGalleryActivity extends RoboActivity{

	@InjectView(R.id.horizontalGallery) Gallery horizontalGallery;
	@InjectView(R.id.container) LinearLayout container;
	private Conteudo mainConteudo;
	private int selectedIndex = 0;
	List<Conteudo> conteudos;
	private ConteudoProvider provider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.slide_gallery);
		handleIntent();
	}

	private void handleIntent() {
		mainConteudo = (Conteudo) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		Conteudo conteudoMain = provider().getRootConteudo();
		Bitmap bitmap = FileUtil.openImage(this, conteudoMain.getId() + ConstUtil.IMAGE_PNG);
		BitmapDrawable bmD = new BitmapDrawable(bitmap);
		container.setBackgroundDrawable(bmD);
		conteudos = provider().getChildConteudos(mainConteudo);
		selectedIndex = getIntent().getIntExtra(ConstUtil.POSITION, 0);
		horizontalGallery.setAdapter(new ConteudoSlideAdapter(this, conteudos));
		horizontalGallery.setSelection(selectedIndex);
	}

	public void onClickPrevious(View view){
		if(selectedIndex >0 ){
			selectedIndex-=1;
			horizontalGallery.setSelection(selectedIndex);
		}

	}

	public void onClickNext(View view){
		if(selectedIndex < conteudos.size() -1){
			selectedIndex +=1;
			horizontalGallery.setSelection(selectedIndex);
		}

	}

	private ConteudoProvider provider(){
		if(provider == null){
			provider = new ConteudoProvider(this);
		}

		return provider;
	}

}
