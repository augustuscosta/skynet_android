package br.com.otgmobile.skynetandroid.activity.IntentProviders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.skynetandroid.activity.ConteudoMapActivity;
import br.com.otgmobile.skynetandroid.activity.ConteudoNewsArticleActivity;
import br.com.otgmobile.skynetandroid.activity.ConteudoNewsListActivity;
import br.com.otgmobile.skynetandroid.activity.ConteudoVideoActivity;
import br.com.otgmobile.skynetandroid.activity.GridGalleryActivity;
import br.com.otgmobile.skynetandroid.activity.SlideGalleryActivity;
import br.com.otgmobile.skynetandroid.database.ConteudoProvider;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.model.TipoDeConteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;

public class IntentProvider {
	
	public static Intent getIntentFromConteudo(Activity context, Conteudo conteudo, Integer position){
		TipoDeConteudo tipoDeConteudo = TipoDeConteudo.fromValue(conteudo.getCategory());
		ConteudoProvider provider = new ConteudoProvider(context);
		Intent intent;
		Bundle bundle;
		switch (tipoDeConteudo) {
		case CONTACT:
			return null;
			
			
		case GALLERY:
			intent = new Intent(context,GridGalleryActivity.class);
			bundle = new Bundle();
			bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
			intent.putExtras(bundle);
			return intent;
			
		case MAP:
			intent = new Intent(context,ConteudoMapActivity.class);
			bundle = new Bundle();
			bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
			intent.putExtras(bundle);
			return intent;
			
		case NEWS:
			intent = new Intent(context,ConteudoNewsListActivity.class);
			bundle = new Bundle();
			bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
			intent.putExtras(bundle);
			return intent;
			
		case SOCIAL:
			
			break;
		case VIDEO:
			intent = new Intent(context,ConteudoVideoActivity.class);
			bundle = new Bundle();
			bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
			intent.putExtras(bundle);
			return intent;
			
		case IMAGEM:
			intent = new Intent(context,SlideGalleryActivity.class);
			bundle = new Bundle();
			Conteudo parent = provider.getParentConteudo(conteudo);
			bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, parent);
			bundle.putInt(ConstUtil.POSITION, position);
			intent.putExtras(bundle);
			return intent;
			
		case NEWS_ARTICLE:
			intent = new Intent(context,ConteudoNewsArticleActivity.class);
			bundle = new Bundle();
			bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
			intent.putExtras(bundle);
			return intent;
		
		}
		return null;
	}

}
