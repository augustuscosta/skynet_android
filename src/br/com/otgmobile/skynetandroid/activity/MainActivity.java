package br.com.otgmobile.skynetandroid.activity;

import java.util.List;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.RelativeLayout;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.activity.IntentProviders.IntentProvider;
import br.com.otgmobile.skynetandroid.adapter.ConteudoMainAdapter;
import br.com.otgmobile.skynetandroid.database.ConteudoProvider;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class MainActivity extends 
GenericActivity<DatabaseHelper>  {
	
	@InjectView(R.id.container) RelativeLayout relativeLayout;
	@InjectView(R.id.HorizontalGallery) Gallery horizontalGallery;
	private List<Conteudo> conteudos;
	private ConteudoProvider provider;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try{
        	Conteudo conteudo = provider().getRootConteudo();
        	Bitmap bitmap = FileUtil.openImage(this, conteudo.getId() + ConstUtil.IMAGE_PNG);
        	BitmapDrawable bmD = new BitmapDrawable(bitmap);
        	conteudos = provider().getChildConteudos(conteudo);
        	horizontalGallery.setAdapter(new ConteudoMainAdapter(this, conteudos));
        	horizontalGallery.setOnItemClickListener(conteudoSelectedListener);
        	relativeLayout.setBackgroundDrawable(bmD);
        }catch(Exception e){
        	LogUtil.e("erro ao pegar o conteudo", e);
        }
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	public void conteudoSelected(Conteudo conteudo) {
			Intent intent = IntentProvider.getIntentFromConteudo(this, conteudo, null);
			startActivity(intent);
	}
	
	private OnItemClickListener conteudoSelectedListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			conteudoSelected(conteudos.get(arg2));
			
		}
	};
	
	
	private ConteudoProvider provider(){
		if(provider == null){
			provider = new ConteudoProvider(this);
		}
		
		return provider;
	}
}
