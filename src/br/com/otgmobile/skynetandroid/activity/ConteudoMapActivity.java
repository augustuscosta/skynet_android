package br.com.otgmobile.skynetandroid.activity;

import java.sql.SQLException;
import java.util.List;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.j256.ormlite.dao.Dao;

public class ConteudoMapActivity extends GenericMapActivity<DatabaseHelper> {
	
	@InjectView(R.id.mapview) MapView mapView;
	private Dao<Conteudo,Long> dao;
	private List<Conteudo> conteudos;
	private Conteudo mainConteudo;
	private MyLocationOverlay myLocationOverlay;
	private MapController mapController;
	
	//Zoom properties
		private int minLat;
		private int maxLat;
		private int minLon;
		private int maxLon;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conteudo_map);
		initializeDao();
		initializeMap();
		handleIntent(getIntent());
	}

	private void initializeMap() {
		// TODO Auto-generated method stub
		
	}

	private void handleIntent(Intent intent) {
		mainConteudo = (Conteudo) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		try{
			conteudos = dao.queryForEq("parentId", mainConteudo.getId());
			createMarkers();
		}catch(Exception e){
			LogUtil.e("erro ao carregar conteudos", e);
		}
	}
	
	
	private void createMarkers() {
		cleanZoomParams();
		if(conteudos == null || conteudos.isEmpty()){
			return;
		}
		
		for (Conteudo conteudo : conteudos) {
			
		}
		
		centerMapByOverlays();
	}
	
	private void configureMaxLatLong(int lat, int lon){
		maxLat = Math.max(lat, maxLat);
		minLat = Math.min(lat, minLat);
		maxLon = Math.max(lon, maxLon);
		minLon = Math.min(lon, minLon);
	}

	private void centerMapByOverlays(){
		if(mapView.getOverlays() == null || mapView.getOverlays().size() < 1)
			return;
		mapController.zoomToSpan(Math.abs(maxLon - minLon), Math.abs(maxLon - minLon));
		mapController.animateTo(new GeoPoint( (maxLat + minLat)/2,(maxLon + minLon)/2 )); 
	}


	private void cleanZoomParams() {
		minLat = Integer.MAX_VALUE;
		maxLat = Integer.MIN_VALUE;
		minLon = Integer.MAX_VALUE;
		maxLon = Integer.MIN_VALUE;
	}

	private void initializeDao() {
		try {
			dao = getHelper().getDao(Conteudo.class);
		} catch (SQLException e) {
			LogUtil.e("erro ao instanciar o dao", e);
		}	
	}

}
