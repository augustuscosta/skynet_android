package br.com.otgmobile.skynetandroid.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.cloud.ConteudoCloud;
import br.com.otgmobile.skynetandroid.database.ConteudoProvider;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class RootLoaderActivity extends Activity {

	private static ConteudoAsyncTask conteudoAsyncTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LogUtil.initLog(this, "SKYNET");
		setContentView(R.layout.splash_loader);
		loadConteudos();
	}

	@Override
	protected void onPause() {
		stopConteudoAsyncTask();
		super.onPause();		
	}

	private void loadConteudos() {
		if(conteudoAsyncTask == null){
			conteudoAsyncTask = new ConteudoAsyncTask(this);
		}

		conteudoAsyncTask.execute();
	}

	private void stopConteudoAsyncTask(){
		if(conteudoAsyncTask != null){
			conteudoAsyncTask.cancel(true);
			conteudoAsyncTask = null;
		}


	}
	private class ConteudoAsyncTask extends AsyncTask<Void,Void,Void>{

		private Context context;
		ProgressDialog dialog;

		ConteudoAsyncTask(Context context){
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(context);
			dialog.setMessage("");
			dialog.setTitle("");
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			ConteudoCloud cCloud = new ConteudoCloud(context);
			try {
				cCloud.getConteudos(context);
			} catch (Exception e) {
				LogUtil.e("erro ao pegar os conteudos da nuvem", e);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.cancel();
			checkContudosAndStartOrClose();

		}

	}


	private void checkContudosAndStartOrClose(){
		Conteudo root = new ConteudoProvider(this).getRootConteudo();

		if(root == null){
			AlertDialog.Builder d = new AlertDialog.Builder(this);
			d.setTitle(R.string.error_conection);
			d.setMessage("Servidor fora do ar sem conteudos para exibir");
			d.setIcon(android.R.drawable.ic_dialog_alert);
			d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					finish();
				}
			});
			d.show();
		}else{
			startActivity(new Intent(this,MainActivity.class));
		}
	}

}
