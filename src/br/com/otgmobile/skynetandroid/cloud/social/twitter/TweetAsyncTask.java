package br.com.otgmobile.skynetandroid.cloud.social.twitter;

import java.io.InputStream;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import roboguice.util.RoboAsyncTask;
import android.app.ProgressDialog;
import android.content.Context;
import br.com.otgmobile.skynetandroid.R;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class TweetAsyncTask extends RoboAsyncTask<Integer> {
	public  ProgressDialog dialog;
	private Context context;
	private String username;
	private String password;
//	private Conteudo conteudo;
	
	protected TweetAsyncTask(Context context,String username, String password, Conteudo conteudo) {
		super(context);
		this.context = context;
		this.username = username;
		this.password = password;
//		this.conteudo = conteudo;
	}
	
	@Override
	protected void onPreExecute() throws Exception {
		super.onPreExecute();
		dialog =ProgressDialog.show(context, "", context.getString(R.string.processing), true, false);
		dialog.setCancelable(false);
	}

	@Override
	public Integer call() throws Exception {
		try {
			DefaultHttpClient client = new DefaultHttpClient();
			UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
			client.getCredentialsProvider().setCredentials( new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT), creds);
			HttpGet request = new HttpGet();
			request.setURI(new URI("https://stream.twitter.com/1/statuses/"));
			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
			in.close();
			}
			catch (Exception e) {
			LogUtil.e("Twitter doInBackground_", e);
			}
			return 1;
	}
	
	@Override
	protected void onSuccess(Integer t) throws Exception {
		super.onSuccess(t);
	}

}
