package br.com.otgmobile.skynetandroid.cloud.social.twitter;

public interface TweetPostCallback {
	
	Boolean tweetPosted(Integer i);

}
