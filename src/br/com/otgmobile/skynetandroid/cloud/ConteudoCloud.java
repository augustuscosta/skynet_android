package br.com.otgmobile.skynetandroid.cloud;

import org.json.JSONArray;

import android.content.Context;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.model.ImageType;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.Session;
import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;

public class ConteudoCloud extends RestClient{
	
	
	private Context context;
	private Dao<Conteudo, Long> dao;
	private ConteudoImageCloud conteudoImageCloud;
	
	public ConteudoCloud(Context context){
		this.context = context;
	}
	
	public JSONArray fetchAllConteudo() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(url);
		execute(RequestMethod.GET);
		getErrorMessage();
		return getJsonObjectArrayFromResponse(getResponse());
	}
	
	public void getConteudos(Context context) throws Exception{
		JSONArray jsonArray = fetchAllConteudo();
		Conteudo obj;
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Conteudo.class);
			Conteudo objFromdb = dao().queryForId(obj.getId());
			if(objFromdb == null || (objFromdb.getUpdatedAt().before(obj.getUpdatedAt()))){
				createOrUpdateAllImages(context, obj);
			}
			dao().createOrUpdate(obj);
		}
	}

	
	public void createOrUpdateAllImages(Context context, Conteudo conteudo) throws Exception{
			conteudoImageCloud().getConteudoICon(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.ICON_PNG);
			conteudoImageCloud().getConteudoImageThumb(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.THUMB_PNG);
			conteudoImageCloud().getConteudoImageMedium(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.MEDIDUM_PNG);
			conteudoImageCloud().getConteudoImage(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.IMAGE_PNG);
			conteudoImageCloud().getConteudoImageLarge(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.LARGE_PNG);
	}
	
	public void getImageOnDemand(Context context, Conteudo conteudo, ImageType imageType) throws Exception{
		switch (imageType) {
		case ICON_PNG:
			conteudoImageCloud().getConteudoICon(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.ICON_PNG);
			
			break;
		case IMAGE_PNG:
			conteudoImageCloud().getConteudoImage(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.IMAGE_PNG);
			break;
			
		case LARGE_PNG:
			conteudoImageCloud().getConteudoImageLarge(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.LARGE_PNG);
	
			break;			
		case MEDIDUM_PNG:
			conteudoImageCloud().getConteudoImageMedium(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.MEDIDUM_PNG);

			break;
		case THUMB_PNG:
			conteudoImageCloud().getConteudoImageThumb(conteudo);
			conteudoImageCloud.storeResponse(context, conteudo.getId()+ConstUtil.THUMB_PNG);
			
			break;
		default:
			break;

		}
		
	}

	private Dao<Conteudo,Long> dao() throws Exception{
		if(dao == null){
			dao = DatabaseHelper.getDatabase(context).getDao(Conteudo.class);
		}
		
		return dao;
	}
	
	private ConteudoImageCloud conteudoImageCloud(){
		if(conteudoImageCloud == null){
			conteudoImageCloud = new ConteudoImageCloud();
		}
		
		return conteudoImageCloud;
	}
	
}
