package br.com.otgmobile.skynetandroid.cloud;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import br.com.otgmobile.skynetandroid.model.Conteudo;

public class ConteudoImageCloud extends RestClient{
	
	private byte[] responseBytes;
	

	public void getConteudoICon(Conteudo conteudo) throws Exception{
		cleanParams();
		String url = conteudo.getImagemIconUrl();
		if(url == null) return;
		setUrl(addSlashIfNeeded(url));
		execute(RequestMethod.GET);
	}
	
	public void getConteudoImageMedium(Conteudo conteudo) throws Exception{
		cleanParams();
		String url = conteudo.getImagemMediumUrl();
		if(url == null) return;
		setUrl(addSlashIfNeeded(url));
		execute(RequestMethod.GET);
	}
	
	public void getConteudoImageLarge(Conteudo conteudo) throws Exception{
		cleanParams();
		String url = conteudo.getImagemLargeUrl();
		if(url == null) return;
		setUrl(addSlashIfNeeded(url));
		execute(RequestMethod.GET);
	}
	
	public void getConteudoImageThumb(Conteudo conteudo) throws Exception{
		cleanParams();
		String url = conteudo.getImagemThumbUrl();
		if(url == null) return;
		setUrl(addSlashIfNeeded(url));
		execute(RequestMethod.GET);
	}
	
	public void  getConteudoImage(Conteudo conteudo) throws Exception{
		cleanParams();
		String url = conteudo.getImagemUrl();
		if(url == null) return;
		setUrl(addSlashIfNeeded(url));
		execute(RequestMethod.GET);		
	}
	
	protected void executeRequest(HttpUriRequest request, String url)
	{
		HttpClient client = new DefaultHttpClient();

		HttpResponse httpResponse;
		InputStream instream;

		try {
			httpResponse = client.execute(request);
			responseCode = httpResponse.getStatusLine().getStatusCode();
			message = httpResponse.getStatusLine().getReasonPhrase();

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {

				instream = entity.getContent();
				responseBytes = convertStreamToBytes(instream);
				instream.close();
			}

		} catch (ClientProtocolException e)  {
			client.getConnectionManager().shutdown();
			message = e.getLocalizedMessage();
		} catch (IOException e) {
			client.getConnectionManager().shutdown();
			message = e.getLocalizedMessage();
		}
	}

	private byte[] convertStreamToBytes(InputStream instream) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(instream);
		ByteArrayBuffer baf = new ByteArrayBuffer(50);
		int current = 0;
		while ((current = bis.read()) != -1) {
			baf.append((byte) current);
		}
		return baf.toByteArray();
	}

	public byte[] getBytes(){
		if(responseCode != 200){
			return null;
		}
		return responseBytes;
	}
	
	

	public void storeResponse(Context context, String localUrl) throws Exception {
		final byte[] logo = getBytes();
		if ( logo == null || logo.length == 0 ) {
			return;
		}
		
		File file = new File(context.getFilesDir(),	localUrl);
		file.createNewFile();
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(logo);
		fos.close();
	}

}
