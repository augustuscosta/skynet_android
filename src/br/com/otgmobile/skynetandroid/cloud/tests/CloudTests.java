package br.com.otgmobile.skynetandroid.cloud.tests;

import java.util.List;

import junit.framework.Assert;

import android.content.Context;
import android.graphics.Bitmap;
import android.test.AndroidTestCase;
import br.com.otgmobile.skynetandroid.cloud.ConteudoCloud;
import br.com.otgmobile.skynetandroid.database.DatabaseHelper;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.util.ConstUtil;
import br.com.otgmobile.skynetandroid.util.FileUtil;

import com.j256.ormlite.dao.Dao;

public class CloudTests extends AndroidTestCase{
	private Context context;
	Dao <Conteudo,Long> dao;
	
	@Override
	protected void setUp() throws Exception {
		context = getContext();
		dao = DatabaseHelper.getDatabase(getContext()).getDao(Conteudo.class);
		super.setUp();
	}
	
	public void testAndPrintJson() throws Exception{
		ConteudoCloud cCloud = new ConteudoCloud(context);
		cCloud.getConteudos(context);
		List<Conteudo> conteudos =  dao.queryForAll();
		Assert.assertNotNull(conteudos);
		Assert.assertTrue(conteudos.size() > 0);
		Bitmap bitmap = FileUtil.openImage(context, conteudos.get(0).getId()+ConstUtil.IMAGE_PNG);
		assertNotNull(bitmap);
		assertTrue(bitmap.getHeight() > 0);
	}
	
	public void testDowloadImages() throws Exception{
		
	}
	

}
