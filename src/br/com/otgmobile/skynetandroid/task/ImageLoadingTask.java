package br.com.otgmobile.skynetandroid.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import br.com.otgmobile.skynetandroid.cloud.ConteudoCloud;
import br.com.otgmobile.skynetandroid.model.Conteudo;
import br.com.otgmobile.skynetandroid.model.ImageType;
import br.com.otgmobile.skynetandroid.util.FileUtil;
import br.com.otgmobile.skynetandroid.util.LogUtil;

public class ImageLoadingTask extends AsyncTask<Void,Void, Bitmap>{
	
	private Context context;
	private Conteudo conteudo;
	private ImageType imageType;
	private ConteudoCloud cloud;
	
	
	public ImageLoadingTask(Context context, Conteudo conteudo, ImageType imageType){
		this.conteudo = conteudo;
		this.context = context;
		this.imageType = imageType;
	}
	
	@Override
	protected void onPreExecute() {
		cloud = new ConteudoCloud(context);
		super.onPreExecute();
	}

	@Override
	protected Bitmap doInBackground(Void... params){	
	Bitmap bitmap = FileUtil.openImage(context, conteudo.getId()+imageType.getValue());
	if(bitmap == null){
		try {
			cloud.getImageOnDemand(context, conteudo, imageType);
			bitmap = FileUtil.openImage(context, conteudo.getId()+imageType.getValue());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar imagem atualizada", e);
		}
	}
	return bitmap;
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
	}

}
