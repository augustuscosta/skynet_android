package br.com.otgmobile.skynetandroid.model;

public enum TipoDeConteudo {
	GALLERY("galeria"),
	NEWS("noticias"),
	NEWS_ARTICLE("noticia"),
	CONTACT("contato"),
	MAP("mapa"),
	SOCIAL("social"),
	VIDEO("videos"),
	IMAGEM("imagem");
	
	private String value;
	
	 TipoDeConteudo(String value){
		this.value = value;
	}
	 
	 public static TipoDeConteudo fromValue(String value) {
		 for(TipoDeConteudo tipoDeConteudo : TipoDeConteudo.values()){
			 if(tipoDeConteudo.value.equalsIgnoreCase(value)) return tipoDeConteudo;
		 }
		 
		 return null;
	}

}
