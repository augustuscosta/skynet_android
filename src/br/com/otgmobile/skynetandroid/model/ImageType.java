package br.com.otgmobile.skynetandroid.model;

public enum ImageType {
	LARGE_PNG("_large.png"),
	IMAGE_PNG("_image.png"),
	MEDIDUM_PNG("_medidum.png"),
	THUMB_PNG ("_thumb.png"),
	ICON_PNG ("_icon.png");
	
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	ImageType(String value) {
		this.value = value;
	}
	
	public static ImageType fromValue(String value){
		for (ImageType imageType : ImageType.values()) {
			if(imageType.getValue().equals(value)) return imageType;
		}
		
		return null;
	}
}
