package br.com.otgmobile.skynetandroid.model;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="conteudo")
public class Conteudo  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2233903272451531740L;

	@DatabaseField(id = true)
	@SerializedName("id")
	private Long id;
	
	@DatabaseField
	@SerializedName("parent_id")
	private Long parentId;
	
 	@DatabaseField
 	@SerializedName("created_at")
	private Date createdAt;
	
	@DatabaseField
	@SerializedName("updated_at")
	private Date updatedAt;
	
	@DatabaseField	
	@SerializedName("categoria")
	private String category;
	
	@DatabaseField
	@SerializedName("descricao")
	private String description;
	
	@DatabaseField
	private String titulo;
	
	@DatabaseField
	private String subtitulo;
	
	@DatabaseField
	@SerializedName("imagem_content_type")
	private String imagemContentType;
	
	@DatabaseField
	@SerializedName("imagem_url")
	private String imagemUrl;
	
	@DatabaseField
	@SerializedName("imagem_url")
	private String imagemThumbUrl;
	
	@DatabaseField
	@SerializedName("imagem_medium_url")
	private String imagemMediumUrl;
	
	@DatabaseField
	@SerializedName("imagem_large_url")
	private String imagemLargeUrl;
	
	@DatabaseField
	@SerializedName("imagem_Icon_url")
	private String imagemIconUrl;
	
	@DatabaseField
	@SerializedName("row_imagem_file_size")
	private Long imagemFileSize;
	
	@DatabaseField
	@SerializedName("row_imagem_updated_at")
	private Date imagemUpdatedAt;
	
	@DatabaseField
	@SerializedName("row_imagem_url")
	private String rowImagemUrl;
	
	@DatabaseField
	@SerializedName("row_imagem_url")
	private String rowImagemThumbUrl;
	
	@DatabaseField
	@SerializedName("row_imagem_medium_url")
	private String rowImagemMediumUrl;
	
	@DatabaseField
	@SerializedName("row_imagem_large_url")
	private String rowImagemLargeUrl;	
	
	@DatabaseField
	private Double latitude;
	
	@DatabaseField
	private Double longitude;
	
	@DatabaseField
	private Boolean social;
	
	@DatabaseField
	private Boolean ativo;
	
	@DatabaseField
	private String url;

	public Conteudo(){
		// To use ORMLite5
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getParentId() {
		return parentId;
	}


	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getSubtitulo() {
		return subtitulo;
	}


	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}


	public String getImagemContentType() {
		return imagemContentType;
	}


	public void setImagemContentType(String imagemContentType) {
		this.imagemContentType = imagemContentType;
	}


	public String getImagemUrl() {
		return imagemUrl;
	}


	public void setImagemUrl(String imagemUrl) {
		this.imagemUrl = imagemUrl;
	}


	public String getImagemThumbUrl() {
		return imagemThumbUrl;
	}


	public void setImagemThumbUrl(String imagemThumbUrl) {
		this.imagemThumbUrl = imagemThumbUrl;
	}


	public String getImagemMediumUrl() {
		return imagemMediumUrl;
	}


	public void setImagemMediumUrl(String imagemMediumUrl) {
		this.imagemMediumUrl = imagemMediumUrl;
	}


	public String getImagemLargeUrl() {
		return imagemLargeUrl;
	}


	public void setImagemLargeUrl(String imagemLargeUrl) {
		this.imagemLargeUrl = imagemLargeUrl;
	}


	public String getImagemIconUrl() {
		return imagemIconUrl;
	}


	public void setImagemIconUrl(String imagemIconUrl) {
		this.imagemIconUrl = imagemIconUrl;
	}


	public Long getImagemFileSize() {
		return imagemFileSize;
	}


	public void setImagemFileSize(Long imagemFileSize) {
		this.imagemFileSize = imagemFileSize;
	}


	public Date getImagemUpdatedAt() {
		return imagemUpdatedAt;
	}


	public void setImagemUpdatedAt(Date imagemUpdatedAt) {
		this.imagemUpdatedAt = imagemUpdatedAt;
	}


	public String getRowImagemUrl() {
		return rowImagemUrl;
	}


	public void setRowImagemUrl(String rowImagemUrl) {
		this.rowImagemUrl = rowImagemUrl;
	}


	public String getRowImagemThumbUrl() {
		return rowImagemThumbUrl;
	}


	public void setRowImagemThumbUrl(String rowImagemThumbUrl) {
		this.rowImagemThumbUrl = rowImagemThumbUrl;
	}


	public String getRowImagemMediumUrl() {
		return rowImagemMediumUrl;
	}


	public void setRowImagemMediumUrl(String rowImagemMediumUrl) {
		this.rowImagemMediumUrl = rowImagemMediumUrl;
	}


	public String getRowImagemLargeUrl() {
		return rowImagemLargeUrl;
	}


	public void setRowImagemLargeUrl(String rowImagemLargeUrl) {
		this.rowImagemLargeUrl = rowImagemLargeUrl;
	}


	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}


	public Boolean getSocial() {
		return social;
	}


	public void setSocial(Boolean social) {
		this.social = social;
	}


	public Boolean getAtivo() {
		return ativo;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


}
